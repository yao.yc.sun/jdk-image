FROM centos:7.5.1804 
LABEL author="sunyao@ichina.com" 

WORKDIR /usr/local/
ENV VERSION 8u191 

RUN yum update -y && yum install java-1.8.0-openjdk-devel -y 

#ENV JAVA_HOME /usr/local/jdk1.8.0_191
#ENV PATH $PATH:$JAVA_HOME/bin:$CATALINA_HOME/bin
#ENV CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar


